from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from pymongo import MongoClient
import os

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["GET", "POST", "PUT", "DELETE"],
    allow_headers=["*"],
)

# Configure MongoDB client
mongo_host = os.getenv('MONGO_HOST', 'localhost')
mongo_port = int(os.getenv('MONGO_PORT', 27017))
mongo_client = MongoClient(f"mongodb://{mongo_host}:{mongo_port}/")
db = mongo_client.test_db

@app.get("/")
def read_root():
    print(mongo_host, mongo_port)
    return {"message": "Hello from FastAPI"}

@app.get("/data")
def get_data():
    # Retrieve data from MongoDB
    collection = db.test_collection
    data = list(collection.find({}, {"_id": 0}))  # Exclude MongoDB's default _id field
    return {"data": data}
